# ds-hw4
Data Structure Lab

Order Management System
In this assignment, you need to write a C++ program to manage the orders in a company, including adding orders, deleting orders, and searching orders. Every order contains its id and the time information, date, within that the order can be finished. Build up a Tree according to its date in every order.

Provided Files

(1) main.cpp
Parse the given input file, execute your functions, and check your answers.

(2) OrderMGMT.h
I. A node structure which you cannot change with elements unsigned id, unsigned date, unsigned leftSize, Node *left, and Node *right.
II. An OrderMGMT class including private and public members and functions that you can change.

(3) OrderMGMT.cpp

Include 4 functions you should program.

1)void OrderMGMT::addOrder(unsigned date, unsigned id)
  Add one order to your order management system according to its date.
  Constrains: If the date of the new order is the same as the date of the order already in your system, you cannot take the order.
  
2)void OrderMGMT::deleteOrders(unsigned start, unsigned end)
  Delete orders whose time information is within a given time interval.
  Constrains: The time interval is a closed interval.
  
3)list<unsigned> OrderMGMT::searchByDate(unsigned start, unsigned end)
  Search your tree and find the orders whose date within a given time interval.
  Store their id in a list.
  Constrains: The time interval is a closed interval.
  The id should be stored in the ordering of their date.
  
4)list<unsigned> OrderMGMT::searchByDateOrdering(unsigned start, unsigned end)
  Search your tree to find the ordering from the start to the end.
  Store their id in a list.
  Constrains: The interval is a closed interval.
  The ordering starts from 1.
  If the end is larger than the maximum number of your nodes, output the nodes ordering from start to the maximum number of your node.
