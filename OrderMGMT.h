/* 
 * DS-HW4 BST Implemention
 * OrderMGMT.h
 * redfish.tbc@gmail.com
 * NCTUee 0110143
 */
#ifndef ORDERMGMT_H
#define ORDERMGMT_H

#include <list>
#include <map>
using namespace std;

struct Node {
    unsigned id;
    unsigned date;
    unsigned leftSize;
    Node *left, *right;
};

// Order Management System consolidating key operational processes.
class OrderMGMT {
    private:
        Node *root;
        unsigned numNode; /* how many node in this tree */
        list<Node> nodeList; /* add and delete node */ 
        list<unsigned> id_list; /* result list */
       
    /* my func */
        static list<Node*> *av; /* pool of available node */
        map<int, Node*> mymap;
        map<int, Node*>::iterator it;
        Node* getNode();
        void  retNode(Node* &);

    public:
        OrderMGMT(){}
        ~OrderMGMT()
        void addOrder(unsigned date, unsigned id);
        void deleteOrders(unsigned start, unsigned end);
        list<unsigned> searchByDate(unsigned start, unsigned end);
        list<unsigned> searchByDateOrdering(unsigned start, unsigned end);

};
#endif

